'''
Created on Dec 4, 2012

@author: jesus

'''
from sys import path
path.append("/var/www/myc/")
from lib.questionaire.BatteryModel import BatteryModel
from lib.questionaire.BatteryResult import BatteryResult
from copy import deepcopy


class CreateQuestionaire():

    def __init__(self):
        self.bModel = BatteryModel()
        self.bResult = BatteryResult()


        self.testQuestion = {
            "description": "How long did you have to wait before a customer service representative at our company began to help you?",
            "id_question": "Q0",
            "type": "single_choice",
            "answers_valid": {"A lot": 5, "Quite a lot":4, "Normal": 3, "A bit":2, "Nothing": 1},
            "answers_order": ["A lot", "Quite a lot", "Normal","A bit", "Nothing"],
            "random_order": False,
            "child_question_conditional":
                [{"value": 0, "id_target_question": "Q1", "condition": "exists"}],
            "page_index": 0,
            "page_order": 0
        }

    def createCustomerModel(self):
        '''
          Test the questionaire data structure composition
        '''
        # Create model
        name = "CustomerModel"
        self.bModel.create(name)
        self.bModel.insertQuestion(self.testQuestion)
        questions = [
                     'How well did the customer service representatives at our company listen to you?',
                     'How eager to help you were the customer service representatives at our company?',
                     'How quickly did the customer service representatives at our company help you?',
                     'How knowledgeable were the customer service representatives at our company?',
                     'How clear was the information the customer service representatives at our company gave you?',
                     'How many of your questions did the customer service representatives at our company resolve?',
                     'How helpful were the customer service representatives at our company?',
                     'Was your experience with customer service at our company better than you expected it to be, worse than you expected it to be, or about what you expected it to be?',
                     ]
        i=1
        for question in questions:
            additionalQuestion = deepcopy(self.testQuestion)
            additionalQuestion['id_question'] = "Q%i" % i
            additionalQuestion["description"] = question
            additionalQuestion['page_order'] = i
            additionalQuestion["child_question_conditional"] = [{"value": 0, "id_target_question": "Q%i" %(i+1), "condition": "exists"}],
            self.bModel.insertQuestion(additionalQuestion)
            i+=1
                        

        # Save
        modelId = self.bModel.save(self.bModel.data)
        
        '''Save the questionaire ID to the HD for testing purposes'''
        with open("testQuestionaireid", "w") as fQuestionaire:
            fQuestionaire.write(str(modelId))
       
if __name__ == "__main__":
    cq = CreateQuestionaire()
    cq.createCustomerModel()
    exit(0)
    
