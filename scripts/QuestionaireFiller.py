'''
Created on Jan 16, 2013
@author: jesus
'''
from sys import path
path.append("/var/www/myc/")
from lib.questionaire.BatteryModel import BatteryModel
from lib.questionaire.BatteryResult import BatteryResult
from lib.questionaire.Questionaire import Questionaire

from math import floor
from datetime import datetime
from pandas import Series
import time
import random


class QuestionaireFiller(object):
    
    QFILL_TYPE_RANDOM = "random"
    QFILL_TYPE_TOP = "top"
    QFILL_TYPE_BOTTOM = "bottom"
    
    def __init__(self, qType=QFILL_TYPE_RANDOM):
        '''
            @param rate: number of questions per second to insert
            @param qType: [random, top, floor]
        '''
        self.bModel = BatteryModel()
        self.bResult = BatteryResult()
        self.qType = qType


    def fillModel(self, modelId, clientId, numberResponses, startTimestamp, endTimestamp ):
        '''
            Fills up the database with answers
            @param modelId: the model to fill up
            @param clientId: the client that will get this data
            @param numberResponses: the number of responses we need to fill
            @param startDate: the start date of the resonses
            @param endDate: the end date of the responses
        '''
        print("Starting with filling")
        #Get the battery model
        batteryModel = self.bModel.load(modelId)
        #Get the period spanned
        timeSpan = floor((endTimestamp-startTimestamp) / float(numberResponses))
        #Get questioneire object
        q = Questionaire()
        answers = self.getProperAnswers(batteryModel, numberResponses)
        
        #Loop number responses
        for i in range(0,numberResponses):
            token = q.createToken(modelId, i, clientId)
            self.bResult.create(token, modelId, i, clientId)
            #Mangle date
            self.bResult.data['created_datetime'] = datetime.fromtimestamp(startTimestamp + (i*timeSpan))
            #Loop questions
            for questionId in batteryModel['questions']:
                #Create new answer
                self.bResult.addAnswer(questionId, answers[questionId][i], 20)
            
            #Add the answer into the db
            self.bResult.save()
            if i%100 == 0:
                print i
        print "All done %i responses created" % numberResponses
        
    def getProperAnswers(self, batteryModel, numberResponses):
        '''
            Get all the answers in a vector depending
            on settings on the class
        '''
        answers = {}
        for question in batteryModel['questions']:
            #Get max and min values
            va = Series(batteryModel['questions'][question]['answers_valid'].values())
            std = va.std()
            minS = va.min()
            maxS = va.max()
            
            if self.qType == "random":
                midRange = va.mean()
            elif self.qType == "top":
                midRange = va.mean() + std
            elif self.qType == "bottom":
                midRange = va.mean() - std
                
            answers[question] = []
            for x in range(0, numberResponses):
                value = floor(random.gauss(midRange,std))
                if value < minS:
                    value = minS
                if value > maxS:
                    value = maxS
                answers[question].append(int(value))
        return answers
    
    

if __name__ == '__main__':
    qf = QuestionaireFiller( QuestionaireFiller.QFILL_TYPE_RANDOM )
    with open("/var/www/myc/scripts/testQuestionaireid", "r") as fQuestionaire:
        modelId = fQuestionaire.read()
    clientIds = range(1,10)
    numberResponses=350
    startTimestamp=time.time() - 2*24*3600
    endTimestamp=time.time()
    modes = [ QuestionaireFiller.QFILL_TYPE_RANDOM, QuestionaireFiller.QFILL_TYPE_TOP, QuestionaireFiller.QFILL_TYPE_BOTTOM ]
    i=0
    for clientId in clientIds:
        qf.qType = modes[i%3]
        qf.fillModel(modelId.strip(), clientId, numberResponses, startTimestamp, endTimestamp)
        i+=1
    exit(0)