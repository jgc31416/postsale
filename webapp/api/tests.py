'''
Created on Mar 15, 2013

@author: jesus
'''

from django.utils import unittest
from lib.api.apiDefault import apiDefault
import json
from lib.db.models.Client import Client

class TestApiDefault(unittest.TestCase):

    def setUp(self):
        
        self.object = apiDefault()
        self.jsonString =json.dumps({     
              "APIKey": "9843hd89h3v3h9ks90w2",
              "sales" : [
                { 
                  "orderId":"834493", 
                  "userId":"432432", 
                  "email":"mymail@gmail.com", 
                  "datetimePurchase":"2013-01-01 20:00:01", 
                  "skus":[ 
                    {  "id":"3214322", 
                       "description":"dhj93ehf29uejkswodj904" } ]
                }]
              })


    def tearDown(self):
        pass

    def testGetAPIDataBad(self):
        self.assertRaises(NameError, self.object.getAPIData, self.jsonString[:-3]) 

    def testGetAPIDataGood(self):
        self.object.getAPIData(self.jsonString)
    
    def testCheckAPIKeyBad(self):
        APIKey = "BADAPIKEY"
        cl = Client()
        self.assertRaises(NameError, self.object.checkAPIKey, cl, APIKey)
    
    def testCheckAPIKeyGood(self):
        '''
        TODO: move to integration tests, it needs values in the database 
        
        APIKey = "GOODAPIKEY"
        cl = Client()
        self.object.checkAPIKey(cl, APIKey)
        '''
        pass



if __name__ == "__main__":
    unittest.main()