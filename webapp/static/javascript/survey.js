// This script is loaded after google Jquery

// Set the totalTimer to the loading page time
var totalTimer =  Math.round(new Date().getTime() / 1000);

// Catch the click on each item (input type="radio", type="text", type="select"
$("input[type='radio']").click( 
	function(){
		var hiddenId = "#id_" + this.name + "_timer";
		var elapsedTime = (Math.round(new Date().getTime() / 1000)) - totalTimer;
		$(hiddenId).val( elapsedTime ) ;
	}	
);

// Catch the submit button to set the total time
$("#submit_survey").click(
	function(){
		var elapsedTime = (Math.round(new Date().getTime() / 1000)) - totalTimer;
		$("#total_timer").val(elapsedTime);
		return true;
	}
);
