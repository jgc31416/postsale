# Create your views here.
from django.shortcuts import render

def index(request):
    '''
        Just present the general site
    '''
    return render(request, 'pricing/index.html', {"request":request})

def contact(request):
    '''
        Just present the general site
    '''
    return render(request, 'pricing/contact.html', {"request":request})

def accounts(request):
    '''
        Return the pricing html
    '''
    return render(request, 'pricing/accounts.html', {"request":request})