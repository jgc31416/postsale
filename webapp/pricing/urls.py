'''
Created on Apr 23, 2013

@author: jesus
'''

from django.conf.urls import patterns, url
urlpatterns = patterns('',
    url(r'contact$', 'pricing.views.contact', name="contact"),
    url(r'accounts$', 'pricing.views.accounts', name="accounts"),
)