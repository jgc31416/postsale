'''
Created on Feb 20, 2013

@author: jesus
'''

from django import forms

class ModifyModelForm(forms.Form):
    action = forms.CharField(widget=forms.HiddenInput())
    model_id = forms.CharField(widget=forms.HiddenInput())
    percentage_flow = forms.IntegerField(max_value = 100, min_value=0, required=False)
    
    
