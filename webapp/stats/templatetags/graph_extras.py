'''
Created on Mar 8, 2013

@author: jesus
'''

from django import template
import logging
import string

register = template.Library()

@register.tag
def do_average_graph(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tagName, scaleMax, avg, avg25, avg75 = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r Tag requires 4 arguments <scaleMax, avg, avg25, avg75> " % token.contents.split()[0])

    node = AverageGraphNode(scaleMax, avg, avg25, avg75)
    
    return node

class AverageGraphNode(template.Node):
    
    YOFFSET_BOTTOM = 101.00
    YMAX = 85.00
    TEXT_OFFSET = 5.0
    
    def __init__(self, scaleMax, avg, avg25, avg75):
        logging.debug("Entering: %s %s %s %s" % (scaleMax, avg, avg25, avg75))
        self.scaleFactor = template.Variable(scaleMax)
        self.avg = template.Variable(avg)
        self.avg25 = template.Variable(avg25) 
        self.avg75 = template.Variable(avg75) 

    def render(self, context):
        avgVal = self.avg.resolve(context)
        avg25Val = self.avg25.resolve(context)
        avg75Val = self.avg75.resolve(context)
        scaleFactorVal = self.scaleFactor.resolve(context)

        scale= AverageGraphNode.YMAX / scaleFactorVal
        avgr = avgVal  * scale 
        avg25r = avg25Val * scale
        avg75r = avg75Val * scale
        
        params = {
            "avg": template.defaultfilters.floatformat(avgVal, 2) ,
            "avgPx": avgr,
            "avg25" : template.defaultfilters.floatformat(avg25Val, 2),
            "avg75" : template.defaultfilters.floatformat(avg75Val, 2),
            "yAvg" : AverageGraphNode.YOFFSET_BOTTOM - avgr,
            "yAvg25" : AverageGraphNode.YOFFSET_BOTTOM - avg25r,
            "yAvg75" : AverageGraphNode.YOFFSET_BOTTOM - avg75r,
        }
        
        params["yAvg25Text"] = params["yAvg25"] + AverageGraphNode.TEXT_OFFSET
        params["yAvg75Text"] = params["yAvg75"] + AverageGraphNode.TEXT_OFFSET
        
        
        if avgVal > avg25Val:
            color = "0f0"
        elif avgVal > avg75Val:
            color = "ff7f00"
        else:
            color = "f00"
        params["color"] = color
        
        templateGraph = string.Template("""
            <svg width="200" height="300" xmlns="http://www.w3.org/2000/svg">
              <g>
              <title>Ejemplo barra</title>
              <rect id="container" height="88" width="46" 
                  y="15"
                  x="2"
                  stroke-width="3" stroke="#000000" fill="#ffffff" rx="1" ry="1"/>
              <text
                  id="svg_7" 
                  fill="#$color" stroke-width="0" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" 
                  x="-40" 
                  y="125"  
                  font-size="20" font-family="Sans-serif" 
                  text-anchor="middle" xml:space="preserve" 
                  font-weight="bold">
                  $avg</text>
            
              <rect
                  id="result" 
                  fill="#$color" 
                  stroke-width="0" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" 
                  x="4" 
                  y="$yAvg" 
                  width="42" 
                  height="$avgPx" 
                  stroke="#000000"/>6
            
              <text 
                  id="txt_25" 
                font-weight="bold" 
                  stroke="#000000" 
                  xml:space="preserve" 
                  text-anchor="middle" 
                  font-family="Sans-serif" font-size="17" 
                  y="$yAvg25Text" 
                  x="35" 
                  stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" 
                  fill="#000000">
                  <title>Top 25%, $avg25 average </title>
                  25%
                  </text>
            
              <line 
                  id="line_25"
                  stroke="#000000" stroke-dasharray="5,5"  
                  y2="$yAvg25" x2="0" 
                  y1="$yAvg25" x1="70" stroke-width="2" fill="none"/>
                  
              <text id="txt_75" font-weight="bold" stroke="#000000" 
                xml:space="preserve" text-anchor="middle" font-family="Sans-serif" 
                font-size="17" 
                  y="$yAvg75Text" 
                  x="35" 
                  stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" 
                  fill="#000000">
                  <title>Top 25%, $avg75 average</title>
                  75%
                  </text>
              <line 
                  id="line_75" stroke="#000000" stroke-dasharray="5,5"  
                  y2="$yAvg75" x2="0" 
                  y1="$yAvg75" x1="70" stroke-width="2" fill="none"/>
              
              </g>
            </svg>

         """)
        value = templateGraph.substitute(params) 
        return value
