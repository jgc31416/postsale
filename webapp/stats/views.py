# Create your views here.
from django.shortcuts import render, redirect

from lib.stats.DataComposer import DataComposer
from lib.questionaire.BatteryModel import BatteryModel 
from forms import ModifyModelForm
from models import ClientInBatteryModel
import logging

def index(request):
    '''
        Shows the welcome screen, 
        gives a user login form
    '''
    return render(request, 'stats/index.html')


def dashboard(request):
    '''
        Shows the user dashboard
    '''
    if request.user.is_authenticated() :
        #get the client_id
        clientId = request.user.client.id
        #get the data
        dc = DataComposer()
        data = dc.getDashboardData(clientId)
            
        #present
        return render(request, 'stats/dashboard.html', {"request":request, "data":data })
    
    else:
        return redirect("/stats/account/login")


def questionaireZoom(request):
    '''
      Shows all the information about the questionaire
    '''
    if request.user.is_authenticated() :
        #get the client_id
        clientId = request.user.client.id
        #get the modelId
        modelId = request.GET['model_id']
        
        #get the data
        dc = DataComposer()
        data = dc.getZoomQuestionaireData(clientId, modelId)
        
       
        #present
        return render(request, 'stats/questionaireZoom.html', {"data":data})
    
    else:
        return redirect("/stats/account/login")
    

def questionZoom(request):
    '''
        Shows a question with the zoom in
    '''
    return render(request, '/stats/questionZoom.html')


def resultZoom(request):
    '''
        Shows a result when user needs to dive in
    '''
    return render(request, '/stats/resultZoom.html')


def modelSettings(request):
    '''
        Shows all the models that are available, and the ones configured to be used
    '''
    if request.user.is_authenticated() :
        #Get the client_id
        clientId = request.user.client.id
        
        if request.method == 'POST':
            form = ModifyModelForm(request.POST)
            if form.is_valid():
                if form.cleaned_data['action'] == "delete":
                    #Delete from the model
                    c = ClientInBatteryModel.objects.filter(client_id=clientId, battery_model_id = form.cleaned_data['model_id'])
                    c.delete() #TODO: change the deletion to a status change
                elif form.cleaned_data['action'] == "add":
                    #Delete from the model
                    c = ClientInBatteryModel(client_id = clientId, 
                                             battery_model_id=form.cleaned_data['model_id'], 
                                             percentage_flow=form.cleaned_data['percentage_flow'],
                                             status = ClientInBatteryModel.ACTIVE)
                    c.save()
                    logging.debug("Record added")
            else:
                logging.debug(form.errors)
                
        return _showModels(request, clientId)
    else:
        return redirect("/stats/account/login")
        

def _showModels(request, clientId):
    '''
        Renders the model settings screen
    '''
    data = {"clientId": clientId}
    bm = BatteryModel()
    
    #Now reload all the forms 
    models = ClientInBatteryModel.objects.filter(client_id=clientId)
    
    #Get the model details from mongo
    data['models'] = bm.addBatteryModelInformation(models) 
    
    #Get all the possible models
    allModels = bm.search({})
    possibleModels = []
    for model in allModels:
        model['id'] = str(model['_id'])
        possibleModels.append(model)
    data['possibleModels'] = possibleModels
    
    return render(request, 'stats/modelSettings.html', {"request":request,"data":data})


import account.forms
import account.views
class LoginView(account.views.LoginView):
    #Set up the login to use email form
    form_class = account.forms.LoginEmailForm

