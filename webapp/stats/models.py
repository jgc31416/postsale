'''
Created on Jan 22, 2013

@author: jesus
'''

#Load all the models
from lib.db.models.Category import *
from lib.db.models.Client import *
from lib.db.models.ClientInCategories import *
from lib.db.models.ClientInBatteryModel import *
from lib.db.models.Notifications import *


#Hook for client creation on user creation
from django.contrib.auth.models import User
from django.db.models.signals import post_save

def create_user_client(sender, instance, created, **kwargs):  
    if created:
        client, created = Client.objects.get_or_create(user=instance, status=0)  

post_save.connect(create_user_client, sender = User)