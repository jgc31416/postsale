'''
Created on Dec 6, 2012

@author: jesus
'''

from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'stats.views.index', name="index"),
    url(r'dashboard$', 'stats.views.dashboard', name="dashboard"),
    url(r'questionZoom$', 'stats.views.questionZoom', name="questionZoom"),
    url(r'questionaireZoom$', 'stats.views.questionaireZoom', name="questionaireZoom"),
    url(r'resultZoom$', 'stats.views.resultZoom', name="resultZoom"),
    url(r'modelSettings$', 'stats.views.modelSettings', name="modelSettings"),
    
)