'''
Created on Dec 13, 2012

@author: jesus
'''
from django import forms
from lib.questionaire.Question import QuestionType
from random import shuffle


class WelcomeForm(forms.Form):
    user_id = forms.CharField(widget=forms.HiddenInput())
    client_id = forms.CharField(widget=forms.HiddenInput())
    model_id = forms.CharField(widget=forms.HiddenInput())


class QuestionairePageForm(forms.Form):
    '''
      Questionaire form
    '''
    error_css_class = 'field_error'
    required_css_class = 'field_required'

    def setTokenId(self, tokenId):
        self.fields['token_id'] = forms.CharField(widget=forms.HiddenInput(), initial=tokenId)

    def addFieldsFromQuestion(self, questions):
        # Add global timer field
        self.fields["total_timer"] = forms.DecimalField(widget=forms.HiddenInput(), initial=0.0)

        # Sort by page order (key)
        for key in sorted(questions.iterkeys()):
            # Add timer field
            self.fields[questions[key]["id_question"] + "_timer"] = forms.CharField(widget=forms.HiddenInput(), initial=0.0)

            if questions[key]["type"] == QuestionType.TYPE_SINGLE_CHOICE:
                if questions[key]['random_order'] == False:
                    # Make choices straight
                    choices = [(questions[key]["answers_valid"][answer], answer) for answer in questions[key]["answers_order"] ]
                else:
                    shuffle(questions[key]["answers_order"])
                    choices = [(questions[key]["answers_valid"][answer], answer) for answer in questions[key]["answers_order"]]

                # Check if it has been answered, if so, preset it
                try:
                    initialValue = questions[key]['answer']['value']
                except KeyError:
                    initialValue = None

                field = forms.ChoiceField(
                                          widget=forms.RadioSelect(),
                                          choices=choices,
                                          initial=initialValue,
                                          label=questions[key]['description'],
                                          )
                self.fields[questions[key]["id_question"]] = field
