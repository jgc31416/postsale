# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.shortcuts import render
from forms import WelcomeForm, QuestionairePageForm
from lib.questionaire.BatteryResult import BatteryResult
from lib.questionaire.Questionaire import Questionaire

# import the logging library
import logging


def index(request):
    '''
    '''
    t = loader.get_template('survey/index.html')
    c = RequestContext(request)
    return HttpResponse(t.render(c))


def welcome(request):
    '''
      Welcomes the user and gives the link to the
      appropriate questionaire
    '''
    user_id = request.GET.get('user_id', '')    # Get user_id
    client_id = int(request.GET.get('client_id', 0))    # Get client_id
    model_id = request.GET.get('model_id', '')    # Get model_id

    if user_id != '' and client_id > 0 and model_id != '':
        # Find results
        br = BatteryResult()
        result = br.search({"user_id": user_id, "client_id": client_id, "model_id": model_id})

        if result is None:
            # Show the new link or
            form = WelcomeForm({"user_id": user_id, "client_id":client_id, "model_id":model_id})    # A bound form
            return render(request, 'survey/welcome.html', {'form': form})
        else:
            # if token already created move him to the survey
            return HttpResponseRedirect('/survey/show?token_id=%s' % result[0]['token_id'])
    else:
        return HttpResponseRedirect("http://google.com")


def show(request):
    '''
    '''
    # Get token
    tokenId = request.GET.get('token_id', '')    # Get token_id
    if tokenId != "":
        qObject = Questionaire()
        questionaire = qObject.getFromToken(tokenId)
        formQuestion = QuestionairePageForm()
        formQuestion.setTokenId(tokenId)
        formQuestion.addFieldsFromQuestion(questionaire['pages'][str(questionaire['last_page'])])
    else:
        return render(request, 'survey/error.html')

    # Render form
    return render(request, 'survey/page.html', {'form': formQuestion})


def new(request):
    '''
      Creates new survey with data passed
    '''

    if request.method == 'POST':    # If the form has been submitted...
        form = WelcomeForm(request.POST)    # A form bound to the POST data
        if form.is_valid():    # All validation rules pass
            # Process the data in form.cleaned_data
            userId = form.cleaned_data['user_id']
            clientId = int(form.cleaned_data['client_id'])
            modelId = form.cleaned_data['model_id']

            # Create new results and get questionaire object
            qObject = Questionaire()
            newQ = qObject.create(clientId, userId, modelId)
            # Create form
            formQuestion = QuestionairePageForm()
            formQuestion.setTokenId(newQ['token_id'])
            formQuestion.addFieldsFromQuestion(newQ['pages'][str(newQ['last_page'])])

            # Render template
            return render(request, 'survey/page.html', {'form': formQuestion})
        else:
            return render(request, 'survey/error.html')
    else:
        return render(request, 'survey/error.html')    # Redirect after POST


def save(request):
    '''
    '''
    try:
        # Get data, parse it
        if request.method == 'POST':    # If the form has been submitted...
            tokenId = request.POST.get('token_id', '')    # Get token_id
            if tokenId == '':
                raise NameError("Bad token id")

            qObject = Questionaire()
            questionaire = qObject.getFromToken(tokenId)
            # Create form
            formQuestion = QuestionairePageForm()
            formQuestion.setTokenId(questionaire['token_id'])
            formQuestion.addFieldsFromQuestion(questionaire['pages'][str(questionaire['last_page'])])

            # Save data if correct
            formQuestion.data = request.POST
            formQuestion.is_bound = True
            formQuestion.full_clean()

            if formQuestion.is_valid():
                ''' '''
                # Get results object
                br = BatteryResult()
                br.loadFromToken(tokenId)
                # Add new results
                for field in formQuestion.visible_fields():
                    timeSpent = formQuestion[field.html_name + "_timer"].value()
                    # logging.debug("%s %s %s" % (field.html_name, field.value(), timeSpent))
                    br.addAnswer(field.html_name, field.value(), timeSpent)
                br.save()
                # Get new questionaire object
                questionaire = qObject.getFromToken(tokenId)

                # Check if the form has all the answers and redirect
                if len(br.data['answers']) == len(qObject.getModel()['questions']):
                    return HttpResponseRedirect("/survey/congratulations")

                # Create new form
                formQuestion = QuestionairePageForm()
                formQuestion.setTokenId(questionaire['token_id'])
                formQuestion.addFieldsFromQuestion(questionaire['pages'][str(questionaire['last_page'])])
            else:
                logging.debug(formQuestion._errors)
                logging.debug("Form is NOT valid")
            # Render form
            return render(request, 'survey/page.html', {'form': formQuestion})
        else:
            raise NameError("Bad method")
    except NameError as e:
        logging.error(e.strerror)
        return render(request, 'survey/error.html')    # Redirect after POST
    '''
    except Exception as e:
        logging.error(e.strerror)
        return render(request, 'survey/error.html')    # Redirect after POST
    '''


def congratulations(request):
    '''
      Congratulate the user for finishing with the questionaire
    '''
    return render(request, 'survey/congratulations.html')
