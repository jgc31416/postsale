'''
Created on Dec 6, 2012

@author: jesus
'''
from django.conf.urls import patterns, url
urlpatterns = patterns('',
    url(r'index$', 'survey.views.index', name="index"),
    url(r'save$', 'survey.views.save', name="save"),
    url(r'new$', 'survey.views.new', name="new"),
    url(r'welcome$', 'survey.views.welcome', name="welcome"),
    url(r'congratulations$', 'survey.views.congratulations', name="congratulations"),
)
