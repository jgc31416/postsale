from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import webapp.stats.views

urlpatterns = patterns("",
    url(r'^$', 'pricing.views.index', name="index"),
    url(r'^survey/', include('survey.urls')),
    url(r'^stats/', include('stats.urls')),
    url(r"^account/login", webapp.stats.views.LoginView.as_view(), name="account_login"),
    url(r"^account/", include("account.urls")),
    url(r'^pricing/', include('pricing.urls')),
)

urlpatterns += staticfiles_urlpatterns()