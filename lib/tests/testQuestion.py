'''
Created on Dec 3, 2012

@author: jesus
'''
import unittest
from lib.questionaire.Question import Question, QuestionType


class TestQuestion(unittest.TestCase):

    def setUp(self):
        self.object = Question()
        self.testDocument = {
            "description": "Esta usted contento con el sitio web?",
            "id_question": "Q1",
            "type": QuestionType.TYPE_OPEN,
            "answers_valid": {"Mucho": 5, "Medianamente": 3, "Poco": 1},
            "answers_order": ["Mucho", "Medianamente", "Poco"],
            "random_order": True,
            "child_question_conditional":
                [{"value": 3, "id_target_question": "Q2", "condition": "="}],
            "page_index": 1,
            "page_order": 1
        }

    def tearDown(self):
        pass

    def testSave(self):
        mongoId = self.object.save(self.testDocument)
        self.object.delete(mongoId)
        self.assertIsNotNone(mongoId)

    def testLoad(self):
        # Create question
        questionId = self.object.save(self.testDocument)
        self.object.load(questionId)
        question = self.object.data
        self.object.delete(questionId)
        self.assertDictContainsSubset(question, self.testDocument, "Question has not been saved properly")

    def testModify(self):
        # Testing adding answer
        self.object.data = self.testDocument
        self.object.addAnswer("Nada", 0, 4)
        self.assertTrue(len(self.object.data['answers_valid']) == 4)
        # Testing random order
        self.object.setAnswerRandom(True)
        self.assertTrue(self.object.data['random_order'] == True)
        # Testing conditions
        self.object.addCondition("Q3", "=", 0)
        self.assertTrue(self.object.findNextQuestion(self.object.data, 0) == "Q3")

    def testFindNextQuestion(self):
        nextQuestionId = self.object.findNextQuestion(self.testDocument, 3)
        self.assertTrue(nextQuestionId == "Q2")

if __name__ == "__main__":
    unittest.main()
