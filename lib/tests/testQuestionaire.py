'''
Created on Dec 4, 2012

@author: jesus
'''
import unittest
from lib.questionaire.BatteryModel import BatteryModel
from lib.questionaire.BatteryResult import BatteryResult
from lib.questionaire.Questionaire import Questionaire
from copy import deepcopy


class TestQuestionaire(unittest.TestCase):

    def setUp(self):
        self.bModel = BatteryModel()
        self.bResult = BatteryResult()
        self.object = Questionaire()

        self.testQuestion = {
            "description": "Esta usted contento con el sitio web?",
            "id_question": "Q1",
            "type": "single_choice",
            "answers_valid": {"Mucho": 5, "Medianamente": 3, "Poco": 1},
            "answers_order": ["Mucho", "Medianamente", "Poco"],
            "random_order": True,
            "child_question_conditional":
                [{"value": 3, "id_target_question": "Q2", "condition": "exists"}],
            "page_index": 0,
            "page_order": 0
        }

    def testGetQuestionaire(self):
        '''
          Test the questionaire data structure composition
        '''
        # Create model
        name = "Battery 2"
        self.bModel.create(name)
        self.bModel.insertQuestion(self.testQuestion)

        secondQuestion = deepcopy(self.testQuestion)
        secondQuestion['id_question'] = "Q2"
        secondQuestion["description"] = "Esta usted contento con sus compras?"
        secondQuestion['page_order'] = 1
        self.bModel.insertQuestion(secondQuestion)
        # Save
        modelId = self.bModel.save(self.bModel.data)

        # Create valid results
        self.bResult.create("Token1", modelId, 1, 1)
        self.bResult.addAnswer("Q1", "3", 25)
        self.bResult.addAnswer("Q2", "5", 20)
        resultId = self.bResult.save()

        # Cross them
        questionaire = self.object.get(modelId, resultId)

        # Check they are fine
        self.assertTrue(len(questionaire['pages']["0"]) == 2)
        self.assertTrue(questionaire['last_page'] == 0)

if __name__ == "__main__":
    unittest.main()
