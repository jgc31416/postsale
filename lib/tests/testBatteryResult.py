'''
Created on Dec 5, 2012

@author: jesus
'''
import unittest
from lib.questionaire.BatteryResult import BatteryResult
from copy import deepcopy


class TestBatteryResults(unittest.TestCase):

    def setUp(self):
        self.object = BatteryResult()

    def tearDown(self):
        pass

    def testCrud(self):
        self.object.create("myTokenId", "myModelId", "myUserId", "myClientId")
        idBR = self.object.save()
        oldObject = deepcopy(self.object.data)
        self.object.load(idBR)
        #Remove timestamps, they are different
        del(oldObject['created_datetime'])
        del(self.object.data['created_datetime'])
        self.assertDictContainsSubset(oldObject, self.object.data)

    def testAddAnswer(self):
        self.object.create("myTokenId", "myModelId","1","1")
        self.object.addAnswer("Q1", 2, 24)
        self.assertTrue("Q1" in self.object.data['answers'])


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
