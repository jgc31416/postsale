'''
Created on Dec 5, 2012

@author: jesus
'''
import unittest
from lib.questionaire.BatteryModel import BatteryModel
from copy import deepcopy


class TestBatteryModel(unittest.TestCase):

    def setUp(self):
        self.object = BatteryModel()
        self.testQuestion = {
            "description": "Esta usted contento con el sitio web?",
            "id_question": "Q1",
            "type": "single_choice",
            "answers_valid": {"Mucho": 5, "Medianamente": 3, "Poco": 1},
            "answers_order": ["Mucho", "Medianamente", "Poco"],
            "random_order": True,
            "child_question_conditional":
                [{"value": 3, "id_target_question": "Q2", "condition": "="}],
            "page_index": 1,
            "page_order": 1
        }

    def tearDown(self):
        pass

    def testCreate(self):
        name = "Battery 1"
        self.object.create(name)
        self.assertTrue(self.object.data['name'] == name)

        # Add question
        self.object.insertQuestion(self.testQuestion)
        self.assertTrue(self.object.countQuestions() == 1)

        # Remove question
        self.object.removeQuestion(self.testQuestion['id_question'])
        self.assertTrue(self.object.countQuestions() == 0)

    def testCrud(self):
        name = "Battery 1"
        self.object.create(name)
        # Add question
        self.object.insertQuestion(self.testQuestion)
        # Save
        objectId = self.object.save(self.object.data)
        oldObject = deepcopy(self.object.data)
        newObject = self.object.load(objectId)
        self.assertDictContainsSubset(oldObject, newObject)
        # Delete
        self.object.delete(objectId)

        
        
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
