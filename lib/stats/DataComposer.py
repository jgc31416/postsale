'''
Created on Feb 1, 2013

@author: jesus
'''

from lib.stats.BatchResultsQuerier import BatchResultsQuerier
from lib.questionaire.BatteryModel import BatteryModel
from lib.questionaire.BatteryResult import BatteryResult
from lib.db.models.Notifications import Notifications 

from datetime import datetime, timedelta
import logging

class DataComposer(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.batchResQ = BatchResultsQuerier()
        
        
    def getDashboardData(self, clientId):
        '''
        Top level function to get the data needed in the dashboard
        it gets disaggregated into each of the pieces needed
        '''
        data = {}
        #Get the model dictionary, mainly for the names...
        bm = BatteryModel()
        batteryModelsDict = bm.getModelsDict()
        
        # Get questionaire index
        dateTarget = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)        
        data['qIndexes'] = self.batchResQ.getBatteryModelClientIndex(clientId, dateTarget)
        # Add the name
        for qindex in data['qIndexes']:
            qindex['name']=batteryModelsDict[qindex['metadata']['model_id']]['name']

        # Get distressed users that entered 24 hrs ago order by date
        data['alerts'] = self.batchResQ.findUsersDistressed(clientId, dateTarget)[0:5]
        for alert in data['alerts']:
            alert['name'] = batteryModelsDict[alert['model_id']]['name']
            alert['id'] = str(alert['_id'])
        
        # Get last activity
        br = BatteryResult()
        lastResultsCursor = br.find({"client_id":clientId}).sort("created_datetime",-1)[0:5]
        lastResults = []
        for result in lastResultsCursor:
            result['model_name'] = batteryModelsDict[result['model_id']]['name']
            result['id'] = str(result['_id'])
            lastResults.append(result)
            
        data['lastActivity'] = lastResults 
        
        # Get notifications
        data['notifications'] = Notifications.objects.filter(client_id = clientId, dismissed=0)
        
        return data
    
    
    def getZoomQuestionaireData(self, clientId, modelId, daysBack = 14):
        data = {}
        # Get 25% and 75% percentiles cut
        data['percentiles'] = self.batchResQ.getBatteryModelPercentiles(modelId, None)
        
        #Get the max and min value for an answer and set it
        bm = BatteryModel()
        bm.load(modelId)
        maxMins = bm.getMaxMinQuestions() 
        globalMaxMin = bm.getMaxMinModel()
        batteryModelsDict = bm.getModelsDict()
        
        # Get average of the last 2 weeks
        dateEnd = datetime.now()
        dateStart = dateEnd - timedelta(days=daysBack)
        averages = self.batchResQ.getBatteryModelAverage(dateStart, dateEnd, clientId, modelId)

        #Set the info in the percentiles
        data['percentiles']['global']['avg'] = averages[modelId]['global']
        data['percentiles']['global']['max'] = globalMaxMin['max']
        data['percentiles']['global']['min'] = globalMaxMin['min']
        data['percentil_global'] = data['percentiles']['global']

        del(data['percentiles']['global'])
        for questionId in averages[modelId]['partial']:
            data['percentiles'][questionId]['max'] = maxMins[questionId]['max']
            data['percentiles'][questionId]['min'] = maxMins[questionId]['min']
            data['percentiles'][questionId]['avg'] = averages[modelId]['partial'][questionId]
            data['percentiles'][questionId]['metadata'] = batteryModelsDict[modelId]['questions'][questionId]
        
        data['modelMetadata'] = batteryModelsDict[modelId]
        
        return data
        