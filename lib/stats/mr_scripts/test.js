
var map = function(){
        var d = this.created_datetime;
        var dateTarget = d.getUTCFullYear() + '-' + (d.getUTCMonth() + 1) + '-' + d.getUTCDate();
        var hourTarget = this.created_datetime.getHours();
        var result = {};
        result[dateTarget] = {};
        result[dateTarget][hourTarget] = 1;
        emit(this.client_id, result);
};


var reduce = function(client_id, resultPassed){       
    var reducedRes = {};                    
    for( var index in resultPassed){                    
        var result = resultPassed[index];                        
        for(var dayT in result ){                               
            if(dayT in reducedRes === false){           
                reducedRes[dayT] = {};                           
            }                               
            for (var hourT in result[dayT]){                                        
                if(hourT in reducedRes[dayT] === true){
                    // Key exists, just add 
                    reducedRes[dayT][hourT] = Number(reducedRes[dayT][hourT]) + Number(result[dayT][hourT]); 
                }else{
                    //Key does not exist, create and initialize 
                    reducedRes[dayT][hourT] = Number(result[dayT][hourT]); 
                } 
            } 
        }
    } 
    return reducedRes; 
};

db.results.mapReduce(
        map,
        reduce,
        { out: "map_reduce_example", }
);