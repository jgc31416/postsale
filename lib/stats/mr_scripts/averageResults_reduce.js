function avgReduce(client_id, resultPassed){       
var reducedRes = {};                    
for( var index in resultPassed){                    
	var result = resultPassed[index];                        
	for(var questionId in result ){                               
		if(questionId in reducedRes === false){           
			reducedRes[questionId] = {};                           
			}                               
		for (var hourT in result[questionId]){                                        
			if(hourT in reducedRes[questionId] === true){
				// Key exists, just add 
				reducedRes[questionId][hourT]['sum'] = Number(reducedRes[questionId][hourT]['sum']) + Number(result[questionId][hourT]['sum']);
				reducedRes[questionId][hourT]['sum_time_took'] = Number(reducedRes[questionId][hourT]['sum_time_took']) + Number(result[questionId][hourT]['sum_time_took']);
				reducedRes[questionId][hourT]['counter'] = Number(reducedRes[questionId][hourT]['counter']) + Number(result[questionId][hourT]['counter']);
				Array.prototype.push.apply(reducedRes[questionId][hourT]['values'], result[questionId][hourT]['values']);
				
			}else{ 
				//Key does not exist, create and initialize 
				reducedRes[questionId][hourT] = { 
						'sum':Number(result[questionId][hourT]['sum']), 
						'sum_time_took':Number(result[questionId][hourT]['sum_time_took']),
						'counter':Number(result[questionId][hourT]['counter']),
						'values': [] 
				}; 
				Array.prototype.push.apply(reducedRes[questionId][hourT]['values'], result[questionId][hourT]['values']);
			} 
		} 
	}
} 
return reducedRes; 
}