function countReduce(client_id, resultPassed){       
	var reducedRes = {};                    
	for( var index in resultPassed){                    
		var result = resultPassed[index];                        
		for(var dayT in result ){                               
			if(dayT in reducedRes === false){           
				reducedRes[dayT] = {};                           
				}                               
			for (var hourT in result[dayT]){                                        
				if(hourT in reducedRes[dayT] === true){
					// Key exists, just add 
					reducedRes[dayT][hourT] = Number(reducedRes[dayT][hourT]) + Number(result[dayT][hourT]); 
				}else{
					//Key does not exist, create and initialize 
					reducedRes[dayT][hourT] = Number(result[dayT][hourT]); 
				} 
			} 
		}
	} 
	return reducedRes; 
}