function avgMap(){
	var d = this.created_datetime;
	var dateTarget = d.getUTCFullYear() + '-' + (d.getUTCMonth() + 1) + '-' + d.getUTCDate();
	var hourTarget = this.created_datetime.getHours();

	for(answerIndex in this.answers){
		var answer = this.answers[answerIndex];
		var result = {};
		result[answerIndex] = {};
		result[answerIndex][hourTarget] = {"sum":answer.value, "sum_time_took": answer.time_spent, "counter":1, "values": [answer.value] };
		emit(this.client_id + "|" + this.model_id + "|" + dateTarget, result)  ;
	}	
}
 

		