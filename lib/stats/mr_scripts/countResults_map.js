function countMap(){
	var d = this.created_datetime;
	var dateTarget = d.getUTCFullYear() + '-' + (d.getUTCMonth() + 1) + '-' + d.getUTCDate();
	var hourTarget = this.created_datetime.getHours();
	var result = {};
	result[dateTarget] = {};
	result[dateTarget][hourTarget] = 1;
	emit(this.client_id + "|" + this.model_id, result);
}

