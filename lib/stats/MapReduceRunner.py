'''
Created on Jan 28, 2013

@author: jesus
'''
from time import time
from os.path import dirname
from bson.code import Code

class MapReduceRunner(object):
    '''
    Runs and measures performance of Map Reduce jobs into 
    '''

    def __init__(self, jobName):
        '''
        Constructor
        '''
        self.startTime = time()
        self.pathMRscripts =dirname( __file__ ) + "/mr_scripts/" 
        self.jobName = jobName
    
    def loadFile(self, typeFile):
        
        if typeFile == "map":
            fileName = self.pathMRscripts + self.jobName + "_map.js"
        elif typeFile == "reduce":
            fileName = self.pathMRscripts + self.jobName + "_reduce.js"
            
        fContents = ""
        
        with open( fileName,"r") as fileScript:
            fContents = fileScript.read()
        
        return Code(fContents)
    
    def run(self, collection, queryToFilter):
        '''
        TODO: set the error handling
        '''
        outputCollection = "mr_%s" % self.jobName
        mapExec = self.loadFile("map")
        reduceExec = self.loadFile("reduce")
        res = collection.map_reduce(mapExec, reduceExec, outputCollection, full_response=True, query=queryToFilter)
        self.logPerformance(res)

    
    def logPerformance(self, mrResults):
        '''
        TODO: this can go to a rdd http://supportex.net/2011/09/rrd-python/
        mrResults = {u'counts': {u'input': 900, u'reduce': 81, u'emit': 8100, u'output': 27}, u'timeMillis': 365, u'ok': 1.0, u'result': u'mw_averageResults'}
        '''
        timeTook = time() - self.startTime
        print "It took: %f" % timeTook
        print mrResults
        
    