'''
Created on Jan 23, 2013

@author: jesus
'''
from datetime import timedelta
import datetime
from lib.questionaire.BatteryResult import BatteryResult
from lib.db.MongoDb import MongoDb
from pandas import DataFrame
import logging

class BatchResultsQuerier(object):
    '''
        This class gets the results from the mongo db
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.connection = MongoDb()
        self.database = self.connection.getDatabase("questionaire")

    
    def getResultsCounter(self, dateStart, dateEnd, clientId, modelId=False):
        '''
            Find all results in given day per clientId and modelId
        '''
        daysToRun = dateEnd - dateStart
        allDates = [dateStart + timedelta(days=x) for x in range(0,daysToRun)]
        
        #Query DB
        query = {"metadata.client_id":"%s" % clientId,
                "metadata.date":{"$in":allDates}}

        if modelId != False:
            query['metadata.model_id'] = "%s" % modelId
        
        res = self.database.stats_countResults.find(query)
        
        #Return
        return res
        
         
    def getResultsAverageDaily(self, dateStart, dateEnd, clientId=None, modelId=None):
        '''
            Fin the results split by battery, client, question and hour for given date
            @param date dateStart: Date start
            @param date dateEnd: Date end
        '''
        
        #Query DB
        #Set dates
        daysToRun = dateEnd - dateStart
        allDates = [ datetime.datetime.combine(dateStart + timedelta(days=x),datetime.time.min) for x in range(-1,daysToRun.days)]
        query = {"metadata.date":{"$in":allDates}}

        #Set clientId
        if clientId is not None:
            if type(clientId)!=list:
                clientId = [clientId]
            clientId = [str(x) for x in clientId]
            query["metadata.client_id"] = {"$in": clientId}
                
        #Set modelId
        if modelId is not None:
            query['metadata.model_id'] = "%s" % modelId
        
        res = self.database.stats_averageResults.find(query)
        
        #Return
        return res
    
    
    def getBatteryModelClientIndex(self, clientId, dateTarget):
        '''
            Calculates the index of a model for a given client and date
            At the moment the index is just the mean
            
            result = {"metadata":{                              "client_id":"xxx",                              "model_id":"xxx",                              "date":"yyyy-month-dd"                              },
                  "day":{
                       "Q1":{
                             "sum":"xxx",                             "counter":"xxx",                             "answer_1":"", #Frequency of answer #                                ...                             "answer_n":"",                             "time_took":"",                             },
                        ...  },
                  "hour_00":{
                      "Q1":{
                             "sum":"xxx",                             "counter":"xxx",                             "answer_1":"", #Frequency of answer #                                ...                             "answer_n":"",                             "time_took":"",                             },
                        ...  },
                  }
             
        '''
        modelAverages = self.getResultsAverageDaily(dateTarget, dateTarget, clientId, modelId = None )
        results = []
        for model in modelAverages:
            totalSum = 0.0
            totalCounter = 0.0
            
            for questionId in model['day']:
                totalSum += model['day'][questionId]['sum']
                totalCounter += model['day'][questionId]['counter']
            
            results.append({"metadata":model['metadata'], "index": totalSum/totalCounter})
        
        return results
    
    
    def getBatteryModelPercentiles(self, modelId, categoryId):
        '''
            Gets the average of the category for a given model
            used in the display of questionaire information questionaireZoon
            returns: {model_id: index, questions:{ ID: {avg: xx, std: xx} } }
        '''
        
        #TODO: implement the category filter: find out the clients ids for the given category
        dateStart = datetime.datetime.now() - datetime.timedelta(days=30)
        dateEnd = datetime.datetime.now()
        rows = self.getResultsAverageDaily(dateStart, dateEnd, None, modelId = modelId)
        rows.sort([("metadata.client_id",1)])
        clientData = {}
        questionIds = {}
        #Group them by client_id
        for row in rows:
            if row['metadata']['client_id'] not in clientData.keys():
                clientData[row['metadata']['client_id']] = {}
            for questionId in row['day']:
                questionIds[questionId] = 1
                if questionId not in clientData[row['metadata']['client_id']].keys():
                    clientData[row['metadata']['client_id']][questionId] = {"counter":row['day'][questionId]['counter'], "total":row['day'][questionId]['sum']}
                else:
                    clientData[row['metadata']['client_id']][questionId]['counter'] += row['day'][questionId]['counter']
                    clientData[row['metadata']['client_id']][questionId]['total'] += row['day'][questionId]['sum']
                
            
        #Set the mean series
        serieMeans = {"global":[]}
        for clientId in clientData:
            globalCounter = 0.0
            globalTotal = 0.0
            for questionId in clientData[clientId]:
                globalCounter += clientData[clientId][questionId]['counter']
                globalTotal += clientData[clientId][questionId]['total'] 
                mean = clientData[clientId][questionId]['total'] / clientData[clientId][questionId]['counter']
                if questionId not in serieMeans.keys():
                    serieMeans[questionId]=[mean]
                else:
                    serieMeans[questionId].append(mean)
                
            serieMeans['global'].append(globalTotal/globalCounter)
                
        #Find the percentiles
        result = {}
        df = DataFrame(serieMeans)
        dfDesc = df.describe()
        for questionId in questionIds:
            result[questionId] = {"p25":dfDesc[questionId]['25%'], "p75":dfDesc[questionId]['75%']}
        result["global"]={"p25":dfDesc['global']['25%'], "p75":dfDesc['global']['75%']}
        
        #Return
        return result
        
    def getBatteryModelAverage(self, dateStart, dateEnd, clientId, modelId ):
        '''
        Gets the average aggregated for given client and given model on
        results between dateStart and dateEnd
        TODO: this function should be splitted in two
        '''
        modelAverages = self.getResultsAverageDaily(dateStart, dateEnd, clientId, modelId)
        
        #Aggregate them by questionaireId
        for average in modelAverages:
            totalCounters = {}
            partialCounters = {}
            for questionId in average['day']:
                try:
                    totalCounters[average['metadata']['model_id']]['sum'] += average['day'][questionId]['sum']
                    totalCounters[average['metadata']['model_id']]['counter'] += average['day'][questionId]['counter']
                except KeyError:
                    if average['metadata']['model_id'] not in totalCounters.keys():
                        totalCounters[average['metadata']['model_id']] = {}    
                    totalCounters[average['metadata']['model_id']]['sum'] = average['day'][questionId]['sum']
                    totalCounters[average['metadata']['model_id']]['counter'] = average['day'][questionId]['counter']
                    
                try:
                    partialCounters[average['metadata']['model_id']][questionId]['sum'] += average['day'][questionId]['sum']
                    partialCounters[average['metadata']['model_id']][questionId]['counter'] += average['day'][questionId]['counter']
                except KeyError:
                    #Build up the data structure
                    if average['metadata']['model_id'] not in partialCounters:
                        partialCounters[average['metadata']['model_id']] = {}
                    if questionId not in partialCounters[average['metadata']['model_id']]:
                        partialCounters[average['metadata']['model_id']][questionId] = {}
                    partialCounters[average['metadata']['model_id']][questionId]['sum'] = average['day'][questionId]['sum']
                    partialCounters[average['metadata']['model_id']][questionId]['counter'] = average['day'][questionId]['counter']
        
        #This can go into another function
        #Get the average of the modelId
        averages = {}
        for modelId in totalCounters:
            averages[modelId] = {'global':totalCounters[modelId]['sum']/totalCounters[modelId]['counter'], 'partial':{}}
        
        for modelId in partialCounters:
            for questionId in partialCounters[modelId]: 
                averages[modelId]['partial'][questionId] = partialCounters[modelId][questionId]['sum']/partialCounters[modelId][questionId]['counter']
            
        return averages
        
    def findUsersDistressed(self, clientId, dateTarget):
        '''
          Finds users that are distressed: more than % less than average users in any of the questions.
        '''
        rows = self.database.results.find( {"client_id":clientId, 
                                            "created_datetime":{"$gte":dateTarget}, 
                                            "is_alertable":True, 
                                            "alert_status": BatteryResult.ALERT_STATUS_PENDING})
        
        return [x for x in rows]
        
    