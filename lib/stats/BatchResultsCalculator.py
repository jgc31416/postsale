'''
Created on Jan 23, 2013

@author: jesus
'''

from lib.db.MongoDb import MongoDb
from copy import deepcopy
from datetime import  timedelta, datetime
import re
from math import pow, sqrt

class BatchResultsCalculator(object):
    '''
        This class creates the records in mongo, with the results from the map reduce
        phase.
    '''
    
    BAD_QUESTION_THRESHOLD = 3

    def __init__(self):
        '''
        Constructor
        '''
        self.connection = MongoDb()
        self.database = self.connection.getDatabase("questionaire")

    
    def calcResultsCounter(self):
        '''
            Find all results in given day, split by clientId and save them
                    
        '''

        # Get results from map-reduce collection
        results = self.database.mr_countResults.find({})
        
        # Generate the running structure
        for result in results:
            records = self.getResultsCounter(result)
            for record in records:
                # Save results in mongo
                res = self.database.stats_countResults.find_one({"metadata.client_id":str(record['metadata']['client_id']), 
                                                     "metadata.model_id":str(record['metadata']['model_id']),
                                                     "metadata.date":record['metadata']["date"]  })
                # Replace contents
                if res is not None:
                    record['_id'] = res['_id']
                
                self.database.stats_countResults.save(record)
                
    
    def getResultsCounter(self, mpResult):
        '''
            Gets a map reduce result and returns it transformed
            into a resultscount record
            
            Comes in:
            { "_id" : "4|50f5853861106e1212b095e7", "value" : { "2013-1-22" : { "14" : 5, "15" : 4, "16" : 4, "17" : 4, "18" : 4, "19" : 4, "20" : 5, "21" : 4, "22" : 4, "23" : 4, "0" : 4 }, "2013-1-23" : { "1" : 4, "2" : 5, "3" : 4, "4" : 4, "5" : 4, "6" : 4, "7" : 4, "8" : 5, "9" : 4, "10" : 4, "11" : 4, "12" : 4, "13" : 4, "14" : 5, "15" : 4, "16" : 4, "17" : 4, "18" : 4, "19" : 4, "20" : 5, "21" : 4, "22" : 4, "23" : 4, "0" : 4 }, "2013-1-24" : { "1" : 4, "2" : 5, "3" : 4, "4" : 4, "5" : 4, "6" : 4, "7" : 4, "8" : 5, "9" : 4, "10" : 4, "11" : 4, "12" : 4, "13" : 4, "14" : 5, "15" : 4, "16" : 4, "17" : 4, "18" : 4, "19" : 4, "20" : 5, "21" : 4, "22" : 4, "23" : 4, "0" : 4 }, "2013-1-25" : { "1" : 4, "2" : 5, "3" : 4, "4" : 4, "5" : 4, "6" : 4, "7" : 4, "8" : 5, "9" : 4, "10" : 4, "11" : 4, "12" : 4, "13" : 4 } } }
            
            Out:
            result = {"metadata":{                          "client_id":"xxx",                          "model_id":"xxx",                          "date":"xxx",                          },              "hours":{                  "0": xxx,                  "1": xxx,                  ...                  "23": xxx,              },              "day": xxx 
        '''
        fixedStructure = {}
        metadata = mpResult['_id'].split("|")
        fixedStructure["metadata"] = { "client_id":metadata[0], "model_id":metadata[1] }
        fixedStructure['day'] = 0
        records = []
        for dayTarget in mpResult['value']:
            resStructure = deepcopy(fixedStructure)
            resStructure['metadata']["date"] = datetime(int(dayTarget.split("-")[0]),int(dayTarget.split("-")[1]),int(dayTarget.split("-")[2]) )
            resStructure['hours'] = mpResult['value'][dayTarget]            
            for hour in resStructure['hours']:
                resStructure['day'] += resStructure['hours'][hour]
            records.append(resStructure)
        return records

            
    
    def calcAverageBatteryDaily(self):
        '''
            Calculate averages for batteries and questions, 
            split by clientId and model_id
        '''
        # Aggregate frequencies and response times
        
        # Get results from map-reduce collection
        results = self.database.mr_averageResults.find({})
        
        # Generate the running structure
        for result in results:
            resStructure = self.getAverageBatteryDaily(result)
        
            # Save results in mongo
            res = self.database.stats_averageResults.find_one({"metadata.client_id":str(resStructure['metadata']['client_id']), 
                                                     "metadata.model_id":str(resStructure['metadata']['model_id']),
                                                     "metadata.date":resStructure['metadata']['date'] })
            
            # Replace contents
            if res is not None:
                resStructure['_id'] = res['_id']
            
            self.database['stats_averageResults'].save(resStructure)

    def getAverageBatteryDaily(self, result):
        '''
        Comes in:
                { "_id" : "4|50f5853861106e1212b095e7|2013-1-25", "value" : { "Q1" : { "1" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "2" : { "sum" : 10, "sum_time_took" : 100, "counter" : 5, "values" : [ 2, 2, 2, 2, 2 ] }, "3" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "4" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "5" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "6" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "7" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "8" : { "sum" : 10, "sum_time_took" : 100, "counter" : 5, "values" : [ 2, 2, 2, 2, 2 ] }, "9" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "10" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "11" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "12" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "13" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] } }, "Q0" : { "1" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "2" : { "sum" : 5, "sum_time_took" : 100, "counter" : 5, "values" : [ 1, 1, 1, 1, 1 ] }, "3" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "4" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "5" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "6" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "7" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "8" : { "sum" : 5, "sum_time_took" : 100, "counter" : 5, "values" : [ 1, 1, 1, 1, 1 ] }, "9" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "10" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "11" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "12" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "13" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] } }, "Q3" : { "1" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "2" : { "sum" : 5, "sum_time_took" : 100, "counter" : 5, "values" : [ 1, 1, 1, 1, 1 ] }, "3" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "4" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "5" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "6" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "7" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "8" : { "sum" : 5, "sum_time_took" : 100, "counter" : 5, "values" : [ 1, 1, 1, 1, 1 ] }, "9" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "10" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "11" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "12" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "13" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] } }, "Q2" : { "1" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "2" : { "sum" : 15, "sum_time_took" : 100, "counter" : 5, "values" : [ 3, 3, 3, 3, 3 ] }, "3" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "4" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "5" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "6" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "7" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "8" : { "sum" : 15, "sum_time_took" : 100, "counter" : 5, "values" : [ 3, 3, 3, 3, 3 ] }, "9" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "10" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "11" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "12" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "13" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] } }, "Q5" : { "1" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "2" : { "sum" : 5, "sum_time_took" : 100, "counter" : 5, "values" : [ 1, 1, 1, 1, 1 ] }, "3" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "4" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "5" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "6" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "7" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "8" : { "sum" : 5, "sum_time_took" : 100, "counter" : 5, "values" : [ 1, 1, 1, 1, 1 ] }, "9" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "10" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "11" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "12" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] }, "13" : { "sum" : 4, "sum_time_took" : 80, "counter" : 4, "values" : [ 1, 1, 1, 1 ] } }, "Q4" : { "1" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "2" : { "sum" : 20, "sum_time_took" : 100, "counter" : 5, "values" : [ 4, 4, 4, 4, 4 ] }, "3" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "4" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "5" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "6" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "7" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "8" : { "sum" : 20, "sum_time_took" : 100, "counter" : 5, "values" : [ 4, 4, 4, 4, 4 ] }, "9" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "10" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "11" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "12" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "13" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] } }, "Q7" : { "1" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "2" : { "sum" : 15, "sum_time_took" : 100, "counter" : 5, "values" : [ 3, 3, 3, 3, 3 ] }, "3" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "4" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "5" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "6" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "7" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "8" : { "sum" : 15, "sum_time_took" : 100, "counter" : 5, "values" : [ 3, 3, 3, 3, 3 ] }, "9" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "10" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "11" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "12" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] }, "13" : { "sum" : 12, "sum_time_took" : 80, "counter" : 4, "values" : [ 3, 3, 3, 3 ] } }, "Q6" : { "1" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "2" : { "sum" : 20, "sum_time_took" : 100, "counter" : 5, "values" : [ 4, 4, 4, 4, 4 ] }, "3" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "4" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "5" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "6" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "7" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "8" : { "sum" : 20, "sum_time_took" : 100, "counter" : 5, "values" : [ 4, 4, 4, 4, 4 ] }, "9" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "10" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "11" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "12" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] }, "13" : { "sum" : 16, "sum_time_took" : 80, "counter" : 4, "values" : [ 4, 4, 4, 4 ] } }, "Q8" : { "1" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "2" : { "sum" : 10, "sum_time_took" : 100, "counter" : 5, "values" : [ 2, 2, 2, 2, 2 ] }, "3" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "4" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "5" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "6" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "7" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "8" : { "sum" : 10, "sum_time_took" : 100, "counter" : 5, "values" : [ 2, 2, 2, 2, 2 ] }, "9" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "10" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "11" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "12" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] }, "13" : { "sum" : 8, "sum_time_took" : 80, "counter" : 4, "values" : [ 2, 2, 2, 2 ] } } } }
        Gets out:
                result = {"metadata":{                              "client_id":"xxx",                              "model_id":"xxx",                              "date":"yyyy-month-dd"                              },
                  "day":{
                       "Q1":{
                             "sum":"xxx",                             "counter":"xxx",                             "answer_1":"", #Frequency of answer #                                ...                             "answer_n":"",                             "time_took":"",                             },
                        ...  },
                  "hour_00":{
                      "Q1":{
                             "sum":"xxx",                             "counter":"xxx",                             "answer_1":"", #Frequency of answer #                                ...                             "answer_n":"",                             "time_took":"",                             },
                        ...  },
                  }
        '''
        resStructure = {}
        metadata = result['_id'].split("|")
        dateTarget = metadata[2]
        resStructure["metadata"] = { "client_id":metadata[0], "model_id":metadata[1], "date":datetime(int(dateTarget.split("-")[0]),int(dateTarget.split("-")[1]),int(dateTarget.split("-")[2]) ) }
        resStructure['day'] = {}

        for questionId in result['value']:
            daySum = 0.0
            dayCounter = 0.0
            dayTime = 0.0
            resStructure["day"][questionId] = {"sum":0,"counter":0,"sum_time_took":0}
            
            for hour in result['value'][questionId]:
                #Check that "hour_%s"%hour exists
                if "hour_%s"%hour not in resStructure:
                    resStructure["hour_%s"%hour] = {}
                     
                resStructure["hour_%s"%hour][questionId] = {"sum":result['value'][questionId][hour]['sum'],
                                                            "counter":result['value'][questionId][hour]['counter'],
                                                            "sum_time_took":result['value'][questionId][hour]['sum_time_took']}
                
                daySum += resStructure["hour_%s"%hour][questionId]['sum']
                dayCounter += resStructure["hour_%s"%hour][questionId]['counter']
                dayTime += resStructure["hour_%s"%hour][questionId]['sum_time_took']
                
                for value in result['value'][questionId][hour]['values']:
                    if "value_%i" % value in resStructure["hour_%s"%hour][questionId]: 
                        resStructure["hour_%s"%hour][questionId]["value_%i" % value] += 1
                    else:
                        resStructure["hour_%s"%hour][questionId]["value_%i" % value] = 1
                    
                    if "value_%i" % value in resStructure["day"][questionId]:
                        resStructure["day"][questionId]["value_%i" % value] +=1
                    else:
                        resStructure["day"][questionId]["value_%i" % value] =1
                        
            resStructure["day"][questionId]["sum"] = daySum
            resStructure["day"][questionId]["counter"] = dayCounter
            resStructure["day"][questionId]["sum_time_took"] = dayTime

        return resStructure

    def calcDistressedUserIndex(self, modelId, clientId, stressIndex=None):
        '''
            Calculates what a distressed user is for the given modelId and clientId
            result = { metadata: { model_id: xxx, client_id: xxx} , index: xxx }
            Stores it in the DB
            :param string modelId
            :param string clientId
            :param float stressIndex 
            
            It could run once per day
            A distressed user could be someone with the average under 1 std deviation of the global average of the month
        '''
        dateTarget = datetime.now() - timedelta(days=30)
        
        # Get model average for the last month/client id/modelId
        results = self.database.stats_averageResults.find({"metadata.model_id":str(modelId), "metadata.client_id":str(clientId), "metadata.date":{"$gt":dateTarget}})
        qfreq = {}
        # Get the frequencies in a list 
        for result in results:
            for question in result['day']:
                try:
                    qfreq[question].append(result['day'][question])
                except KeyError:
                    qfreq[question] = [result['day'][question]]
        
        qdata = {}
        for question in qfreq:
            sumTotal = 0
            count = 0
            valueCounter = []
            for resAvg in qfreq[question]:
                sumTotal += resAvg['sum']
                count += resAvg['counter']
                for keyValue in resAvg.keys():
                    if re.match("value_", keyValue):
                        value = int(keyValue.split("_")[1])
                        valueCounter.append((value,resAvg[keyValue])) #We need this to compute the stdDev 
            meanQ = sumTotal / float(count)
            #Get std deviation
            stdSum = 0
            for valuePair in valueCounter:
                # Get average and std deviation
                stdSum += pow( (valuePair[0] - meanQ) * valuePair[1],2 )
            stdQ = sqrt(stdSum)/count
            qdata[question] = {"mean":meanQ, "std":stdQ}
        
        yesterday = datetime.now() - timedelta(days=1)
        # Get results
        results = self.database.results.find({"client_id":clientId, "model_id": modelId, "created_datetime":{"$gt":yesterday}})
        
        # Mark results
        for result in results:
            if self.isResultDistressed(result, qdata):
                result['is_alertable'] = True
                self.database.results.save(result)
                
    
    
    def isResultDistressed(self, result, questionData):
        '''
          Returns if the result is distressed or not
        '''
        totalBadQuestions = 0 
        for question in result['answers']:
            if result['answers'][question]['value'] < (questionData[question]['mean'] - questionData[question]['std']):
                totalBadQuestions += 1
        
        if totalBadQuestions > BatchResultsCalculator.BAD_QUESTION_THRESHOLD:
            return True
        else:
            return False
        