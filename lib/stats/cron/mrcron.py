'''
Created on Jan 28, 2013

@author: jesus

Runs map reduce crons to get stats up to date, takes the data for the last 24 hrs

'''
from datetime import datetime, timedelta
from os.path import abspath, dirname
from sys import path, argv

path.append(dirname(dirname(dirname(dirname(abspath(__file__))))))

from lib.db.MongoDb import MongoDb
from lib.stats.MapReduceRunner import MapReduceRunner


if __name__ == '__main__':
    
    if len(argv) < 2:
        print "usage: mrcron.py <MRScriptName>"
        exit(-1)
    
    scriptName = argv[1]
    
    connection = MongoDb()
    collection = connection.getDatabase("questionaire")['results']
    mrr = MapReduceRunner(scriptName)
    
    daysBack = timedelta(days = 30)
    dateEnd = datetime.today()
    dateStart = dateEnd - daysBack
    query = { "$and": [
                    {"created_datetime": { "$gt": dateStart } },
                    {"created_datetime": { "$lt": dateEnd } },
                ]
             }
    
    mrr.run(collection, query)
    
    