'''
Created on Jan 28, 2013

@author: jesus

Runs batch result crons

'''
from os.path import dirname,abspath 
from sys import path
import logging
 
path.append(dirname(dirname(dirname(dirname(abspath(__file__))))))
from lib.stats.BatchResultsCalculator import BatchResultsCalculator
from lib.db.models.ClientInBatteryModel import ClientInBatteryModel 

if __name__ == '__main__':
    
    brc = BatchResultsCalculator()
    #Calc counters
    brc.calcResultsCounter()
    #Calc averages
    brc.calcAverageBatteryDaily()
    

    #Get model id and user id for all the customers
    clientBatteryList = ClientInBatteryModel.objects.filter(status = ClientInBatteryModel.ACTIVE)
    for data in clientBatteryList:
        try:
            brc.calcDistressedUserIndex(data.battery_model_id, data.client_id)
        except:
            logging.exception("Error processing cron DistressedUserIndex")
            