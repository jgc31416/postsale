'''
Created on Mar 15, 2013

@author: jesus
'''

import json
from lib.db.models.Client import Client

class apiDefault(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    def parse(self, apiString):
        '''
            Parse the Json comming from the API,
            checks the API key and gets the clientId
        '''

        apiData = self.getAPIData(apiString)
        
        cl = Client()
        client = self.checkAPIKey(cl, apiData['APIKey'])
        
        apiData['clientId'] = client.id
        
        return apiData
        
        
    def getAPIData(self, apiString):
        #Get API key
        try:
            apiData = json.loads(apiString, "utf8")
        except:
            raise NameError("Bad JSON format")
        return apiData
    
    
    def checkAPIKey(self, clientObject, APIKey):
        '''
          Get client from APIKey if exists
        '''
        client =  clientObject.findClientByAPIKey(APIKey)
        if len(client) <=0:
            raise NameError("API non found")
        else:
            return client[0]
