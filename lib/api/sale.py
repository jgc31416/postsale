'''
Created on Mar 15, 2013

@author: jesus
'''
from lib.api.apiDefault import apiDefault
from lib.db.MongoDb import MongoDb
import logging
from datetime import datetime

class SaleValidationException( Exception ):
    def __init__(self):
        pass

class Sale(apiDefault):
    '''
    classdocs
    '''

    STATUS_NEW = 1
    STATUS_SENT = 2
    STATUS_ERROR = 3

    def __init__(self):
        '''
            Constructor
        '''
        #Get mongo connection
        self.dbConn = MongoDb().getDatabase("sales")
    
    def add(self, sale):
        '''
            Add a valid sale into the database
        '''
        try:
            #Check validity
            self.validate(sale)
            #Add to the database
            self.dbConn['orders'].add(sale)
        except SaleValidationException:
            raise
        except Exception as ex:
            logging.exception(ex)
            raise
        
    def validate(self, sale):
        '''
            Validate a sale.
            Correct structure:
                client_id: int,
                amount: float,
                date: datetime,
                order_id: string < 100,
                email: string < 150, email type
                product_desc: string < 250,
                
            Throws an error if something is wrong
        '''
        cleanSale = {}
        if int(sale['client_id']) <= 0:
            raise SaleValidationException("Bad client id")
        cleanSale['client_id'] = int(sale['client_id'])
            
        if float(sale['amount']) <= 0.0:
            raise SaleValidationException("Bad amount id")
        cleanSale['amount'] = float(sale['amount'])
        
        #Check for iso format
        dateFormat = "%Y-%m-%d %H-%M-%S"
        try:
            dateSale = datetime.strptime(sale['date'], dateFormat)
        except ValueError:
            raise SaleValidationException("Bad date format")
        cleanSale['data']=dateSale
        
        #Check for strings
        stringFields = {"order_id":100, "email":150, "product_desc":250}
        for field, maxLenght in stringFields.items():
            if len(sale[field]) > maxLenght:
                raise SaleValidationException("Field %s exceedes length" % field)
            else:
                cleanSale[field]=sale[field]
        
        return cleanSale
        
        
    def get(self, saleId):
        '''
            Get a sale by its ids 
        '''
        
    def getByClientId(self, clientId):
        '''
          Get sales by ClientID
        '''
        
    def getBySaleStatus(self, clientId, status):
        '''
          Get sales with given status
        '''

