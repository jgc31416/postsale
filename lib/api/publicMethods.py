'''
Created on Mar 15, 2013

@author: jesus
'''
from lib.api.apiDefault import apiDefault

class Sales(apiDefault):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
    
    def validate(self, sale):
        '''
            Validate a sale
        '''
    
    def add(self, validSale):
        '''
            Add a valid sale into the database
        '''
        
    def getByClientId(self, clientId):
        '''
          Get sales by ClientID
        '''
        
    def getQuestionaireNotSent(self, clientId):
        '''
          Get sales from which no mail has been sent yet
        '''

