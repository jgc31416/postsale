'''
Created on Nov 27, 2012

@author: jesus
'''

import pymongo

class MongoDb(object):
    '''
      classdocs
    '''
    
    
    def __init__(self):
        '''
          Constructor
        '''
    
    def getConnection(self, name):
        '''
          Get the database connection
        '''
        conn = pymongo.Connection()
        return conn
    
    def getDatabase(self, name):
        conn = self.getConnection("production")
        return conn[name]