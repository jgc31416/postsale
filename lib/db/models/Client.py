from django.db import models
from django.contrib.auth.models import User
from lib.db.TimestampField import TimestampField


# Create your models here.
 
class Client(models.Model):
    '''
      Client model attached to user
    '''
    user = models.OneToOneField(User)  
    first_name = models.CharField(max_length=40,blank=True)
    last_name = models.CharField(max_length=40,blank=True)
    addresss = models.CharField(max_length=100,blank=True)
    zip_code = models.CharField(max_length=100,blank=True)
    country = models.CharField(max_length=100,blank=True)
    city = models.CharField(max_length=100,blank=True)
    phone_number = models.CharField(max_length=100,blank=True)
    status = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add = True)
    modified_timestamp = TimestampField()
    api_key = models.CharField(max_length=50)


    def findClientByAPIKey(self, APIKey):
        #client = Client.objects.raw("select * from stats_client")
        client = Client.objects.filter(api_key=APIKey).exclude(status=0)
        
        return client

    class Meta:
        app_label = 'stats'
    