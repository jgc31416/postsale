'''
Created on Jan 22, 2013

@author: jesus
'''
from django.db import models
from lib.db.TimestampField import TimestampField


class Category(models.Model):
    '''
    classdocs
    '''
    name = models.CharField(max_length=40)
    status = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add = True)
    modified_timestamp = TimestampField()
    
    class Meta:
        app_label = 'stats'
        