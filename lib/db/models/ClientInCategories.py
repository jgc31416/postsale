'''
Created on Jan 22, 2013

@author: jesus
'''
from django.db import models
from lib.db.TimestampField import TimestampField


class ClientInCategories(models.Model):
    '''
    classdocs
    '''
    client_id = models.IntegerField()
    category_id = models.IntegerField()
    modified_timestamp = TimestampField()

    class Meta:
        app_label = 'stats'