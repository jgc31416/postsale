'''
Created on Feb 20, 2013

@author: jesus
'''

from django.db import models
from lib.db.TimestampField import TimestampField


class ClientInBatteryModel(models.Model):
    '''
    classdocs
    '''
    ACTIVE = 1
    INACTIVE = 0
    STATUS_CHOICES = (
        (ACTIVE, "active"),
        (INACTIVE, "inactive")
    )
    
    client_id = models.IntegerField()
    battery_model_id = models.CharField(max_length=50)
    percentage_flow = models.IntegerField() # % of users allowed to fill the form from total 
    modified_timestamp = TimestampField()
    status = models.IntegerField(choices=STATUS_CHOICES, default=INACTIVE)

    class Meta:
        app_label = 'stats'
        
    