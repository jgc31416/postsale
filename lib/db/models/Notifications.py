'''
Created on Mar 1, 2013

@author: jesus
'''

from django.db import models
from lib.db.TimestampField import TimestampField


class Notifications(models.Model):
    '''
    classdocs
    '''
    
    title = models.CharField(max_length=40)
    description = models.CharField(max_length=400)
    dismissed = models.IntegerField(default=0)
    send_by_mail = models.IntegerField(default=0) #If we need to send it by mail
    is_sent = models.IntegerField(default=0)
    client_id = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add = True)
    valid_until_date = models.DateTimeField()
    modified_timestamp = TimestampField()
    
    
    class Meta:
        app_label = 'stats'