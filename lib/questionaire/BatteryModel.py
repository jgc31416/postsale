'''
Created on Nov 27, 2012

@author: jesus
'''
from lib.db.MongoDb import MongoDb
from bson import ObjectId
from datetime import datetime
import logging
import sys

class BatteryModel(object):
    '''
    classdocs
    model:
      _id
      name
      date_created
      questions {questionId:{ question }}
    '''
    data = {}

    def __init__(self):
        '''
        Constructor
        '''
        mdb = MongoDb()
        self.dbConn = mdb.getDatabase("questionaire")

    def save(self, model):
        '''
        '''
        objectId = self.dbConn.batteryModels.save(model, safe=True)
        return objectId

    def load(self, modelId):
        '''
        '''
        model = self.dbConn.batteryModels.find_one({"_id": ObjectId(modelId)})
        if model is None:
            raise NameError("Model not found")
        else:
            self.data = model
            return model

    def delete(self, modelId):
        self.dbConn.batteryModels.remove({"_id": ObjectId(modelId)})

    def search(self, params):
        '''
        '''
        results = self.dbConn.batteryModels.find(params)
        return results

    def create(self, name):
        '''
          Create new battery structure
        '''
        dateCreated = datetime.now()
        self.data = {"name": name, "date_created": dateCreated.isoformat(), "questions": {}}
        return self.data

    def insertQuestion(self, question):
        '''
        '''
        self.data['questions'][question['id_question']] = question

    def removeQuestion(self, questionId):
        '''
        '''
        # Look in the list for the index
        del(self.data['questions'][questionId])
        return self.data

    def checkBatteryIntegrity(self, model):
        '''
          Checks wether we can reach all the questions by
          following parent/child relationships
        '''

    def countQuestions(self):
        return len(self.data['questions'])
    
    
    def addBatteryModelInformation(self, objectsModel):
        '''
        :var ObjectsModel is an object that has the property model_id set 
        '''
        modelsInfo = []
        for objectItem in objectsModel:
            try:
                model = objectItem.__dict__
                modelInfo = self.load(objectItem.battery_model_id)
                model['name']=modelInfo['name']
                modelsInfo.append(model)
            except:
                logging.error(sys.exc_info())
                raise Exception("Cannot find model info")
        return modelsInfo
    
    def getModelsDict(self):
        '''
        Gets all the models in a dictionary
        '''
        
        batteryModels = self.search({})
        modelDict = {}
        for model in batteryModels:
            modelDict[str(model['_id'])] = model
        
        return modelDict
    
    def getMaxMinQuestions(self):
        '''
        Returns a dictionary with the minimum and the maximum answer in a model
        '''
        values = {}
        response = {}
        for questionId in self.data['questions']:
            values[questionId] = []
            for answerName in self.data['questions'][questionId]['answers_valid']:
                values[questionId].append(self.data['questions'][questionId]['answers_valid'][answerName])
            response[questionId] = {'min': min(values[questionId]), 'max': max(values[questionId])}
            
        return response
        
    def getMaxMinModel(self):
        '''
          Get the maximum and minimum value in the model
          This is used in the representation of the averages
        '''
        values = {}
        maxV = []
        minV = []
        for questionId in self.data['questions']:
            values[questionId] = []
            for answerName in self.data['questions'][questionId]['answers_valid']:
                values[questionId].append(self.data['questions'][questionId]['answers_valid'][answerName])
                
            maxV.append(max(values[questionId]))
            minV.append(min(values[questionId]))
            
        return {'min':min(minV) , 'max':max(maxV)}
        