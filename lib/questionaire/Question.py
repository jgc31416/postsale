'''
Created on Nov 27, 2012

@author: jesus
'''
from lib.db.MongoDb import MongoDb
from bson import ObjectId

class QuestionType(object):
    '''
    Holds the type of questions we can have
    '''
    TYPE_OPEN = "open"
    TYPE_SINGLE_CHOICE = "single_choice"
    TYPE_MULTIPLE_CHOICE = "multiple_choice"    

class Question(object):
    '''
    classdocs
    Model:
        _id: ObjectId
        id_question: id   #Simple way to remember the question
        description: text
        type: {open, choice}
        answers_valid: { description: value, ... }
        answers_order: [ description, ... ]
        random_order: T/F
        child_question_conditional:
            [ {value:xxx, id_target_question:xxx, condition=[exists|>|<|=]},...]
        page_index: #,
        page_order: #
    '''


    data = {}

    def __init__(self):
        '''
          Constructor
        '''
        mdb = MongoDb()
        self.dbConn = mdb.getDatabase("questionaire")

    def load(self, questionId):
        '''
          Load the question
        '''
        self.data = self.dbConn.questions.find_one({"_id": ObjectId(questionId)})

    def save(self, questionModel):
        '''
          Saved the passed question
        '''
        mongoId = self.dbConn.questions.save(questionModel, safe=True, manipulate=True)
        return mongoId

    def delete(self, mongoId):
        '''
          Delete a question
        '''
        self.dbConn.questions.remove(ObjectId(mongoId))
        return True

    def searchQuestion(self, params):
        '''
          Searchs in questions given the params
        '''
        Questions = self.dbConn.questions.find(params)
        return Questions

    def findNextQuestion(self, question, answerValue):
        '''
          Returns the id of the next question given a result
        '''

        nextId = ""
        for questionConditional in question['child_question_conditional']:
            if questionConditional['condition'] == "exists":
                if answerValue is not None:
                    nextId = questionConditional['id_target_question']
                    break
            elif questionConditional['condition'] == ">":
                if answerValue > questionConditional['value']:
                    nextId = questionConditional['id_target_question']
                    break
            elif questionConditional['condition'] == "=":
                if answerValue == questionConditional['value']:
                    nextId = questionConditional['id_target_question']
                    break
            elif questionConditional['condition'] == "<":
                if answerValue < questionConditional['value']:
                    nextId = questionConditional['id_target_question']
                    break

        if nextId == "":
            raise NameError("Next question not found")
        else:
            return nextId

    def create(self, idQuestion=None,
                     description=None,
                     typeQuestion=QuestionType.TYPE_SINGLE_CHOICE,
                     pageIndex=0,
                     pageOrder=0):
        '''
          Creates a new question
        '''
        self.data = {
            "id_question": idQuestion,    # Simple way to remember the question
            "description": description,
            "type": typeQuestion,
            "answers_valid": {},
            "answers_order": [],
            "random_order": False,
            "child_question_conditional": [],
            "page_index": pageIndex,
            "page_order": pageOrder
        }

    def addAnswer(self, description, value, orderIndex):
        '''
          Adds a new answer
        '''
        self.data['answers_valid'][description] = value
        self.data['answers_order'].insert(orderIndex, description)

    def addCondition(self, idTargetQuestion, condition, value):
        '''
          Adds a new condition
        '''
        newCondition = {"value": value,
                        "id_target_question": idTargetQuestion,
                        "condition": condition}
        self.data['child_question_conditional'].append(newCondition)

    def setAnswerRandom(self, value):
        '''
          Sets whether the answer is in random order or not
        '''
        self.data['random_order'] = value

