'''
Created on Dec 4, 2012

@author: jesus
'''
from copy import deepcopy
from lib.questionaire.BatteryModel import BatteryModel
from lib.questionaire.BatteryResult import BatteryResult
from hashlib import sha1


class Questionaire(object):
    '''
        Get the questionaire
        model_id: ObjectId
        pages: {page_index: 
                 {page_order:
                    {
                    "_id": xxx
                    "description": "Esta usted contento con el sitio web?",
                    "id_question": "Q1",
                    "type": Question.TYPE_OPEN,
                    "answers_valid": {"Mucho": 5, "Medianamente": 3, "Poco": 1},
                    "answers_order": ["Mucho", "Medianamente", "Poco"],
                    "answer": {
                        "value":xxx,
                        "time_spent":xxx
                    },
                    "random_order": True,
                    "child_question_conditional":
                        [{"value": 3, "id_question": "Q2", "condition": "="}],
                    "page_index": 0,
                    "page_order": 0
                    }
                }
            }
        is_alertable: False,        # Is this answer too low for the client so he needs to be alerted?
        alert_status: 0,        # This can take the following statuses: 0 pending, 1 dealt
        last_page: None,
        user_id: xxx,
        client_id: xxx,
        token_id: xxx,
        created_datetime: datetime.now()
    '''

    def __init__(self):
        '''
        '''

    def getModel(self):
        '''
          Returns the loaded model
        '''
        if self.model is not None:
            return self.model

    def get(self, batteryModelId, batteryResultId):
        '''
          Get a questionaire structure
        '''

        bm = BatteryModel()
        self.model = bm.load(batteryModelId)
        modelWithResults = deepcopy(self.model)

        if batteryResultId is not None:
            br = BatteryResult()
            results = br.load(batteryResultId)
            
            
            # Set the results in the model
            for questionId in results['answers']:
                modelWithResults['questions'][questionId]['answer'] = {
                                                "value": results['answers'][questionId]["value"],
                                                "time_spent": results['answers'][questionId]["value"]}
            # Sort out the questionaire object
            questionaire = {"pages": {}, "model_id": modelWithResults['_id'],
                        "user_id": results['user_id'],
                        "client_id": results['client_id'],
                        "token_id": results['token_id'],
                        "created_datetime": results['created_datetime']}
            for questionId in modelWithResults['questions']:
                pageIndex = modelWithResults['questions'][questionId]['page_index']
                pageOrder = modelWithResults['questions'][questionId]['page_order']
                if str(pageIndex) not in questionaire['pages']:
                    questionaire['pages']["%i" % pageIndex] = {}
                questionaire['pages']["%i" % pageIndex]["%i" % pageOrder] = deepcopy(modelWithResults['questions'][questionId])
            
            #Get alertable fields in questionaire
            questionaire['is_alertable'] = results['is_alertable']
            questionaire['alert_status'] = results['alert_status']
                
            self.data = questionaire
            # Set the last page
            self.data['last_page'] = self.getLastPage()
    
            return self.data        
        else:
            raise NameError("Result not found")
            
    def getFromToken(self, resultToken):
        '''
          Loads a questionaire from the result token
        '''
        br = BatteryResult()
        result = br.loadFromToken(resultToken)
        if result is not None:
            return self.get(result['model_id'], result['_id'])

    def getLastPage(self):
        '''
        Figures out the last page from last empty answer
        '''

        lastPage = 0
        for pageIndex in range(0, len(self.data['pages']) - 1):
            for pageOrder in range(0, len(self.data['pages'][pageIndex]) - 1):
                if len(self.data['pages'][pageIndex][pageOrder]['answer']) == 0:
                    return pageIndex
        return lastPage

    def create(self, clientId, userId, modelId):
        '''
          Creates a new questionaire object and returns it
          @param clientId: client id
          @param userId: user id
          @param modelId: model id
          @return: tokenId
        '''
        bResult = BatteryResult()
        tokenId = self.createToken(modelId, userId, clientId)
        bResult.create(tokenId, modelId, userId, clientId)
        resultId = bResult.save()
        questionaire = self.get(modelId, resultId)

        return questionaire
    
    def createToken(self, modelId, userId, clientId):
        return sha1("%s|%s|%s" % (modelId, userId, str(clientId))).hexdigest()