'''
Created on Nov 27, 2012

@author: jesus
'''
from lib.db.MongoDb import MongoDb
from bson import ObjectId
from datetime import datetime
from time import time


class BatteryResult(object):
    '''
    Data structure
        "_id":xxx,
        "token_id":xxx,
        "model_id":xxx,
        'user_id': xxx,
        'client_id': xxx,
        "answers": {"question_id":{
                        "value":xxx,
                        "time_spent":xxx}
                        },
        'created_datetime': datetime.now(), 
        'is_alertable': False,        # Is this answer too low for the client so he needs to be alerted?
        'alert_status': 0,        # This can take the following statuses: 0 pending, 1 dealt
        'timestamp': time.time()
    '''
    
    ALERT_STATUS_PENDING = 0 # This is a value for alert_status, pending
    ALERT_STATUS_DEALT = 1 # This is a value for alert_status, dealt
    
    data = {}

    def __init__(self):
        '''
          Constructor
        '''
        # Load the model
        mdb = MongoDb()
        self.dbConn = mdb.getDatabase("questionaire")
        self.collection = self.dbConn.results

    def load(self, resultsId):
        '''
          Load the results from db
        '''
        self.data = self.collection.find_one({"_id": ObjectId(resultsId)})
        return self.data

    def loadFromToken(self, tokenId):
        '''
          Load the results from db
        '''
        self.data = self.collection.find_one({"token_id": tokenId})
        return self.data

    def search(self, params):
        '''
            Searchs for given params in the database
        '''
        results = self.collection.find_one(params)
        return results

    def find(self, params):
        '''
            
        '''
        results = self.collection.find(params)
        return results
    
    def save(self):
        '''
          Save the battery result in the DB
        '''
        self.data['timestamp'] = time()
        idAnswer = self.collection.save(self.data, safe=True)
        return idAnswer

    def delete(self, resultsId):
        '''
          Remove from the DB
        '''
        self.collection.remove({"_id": ObjectId(resultsId)})

    def create(self, tokenId, modelId, userId, clientId):
        '''
          Creates new battery result
        '''
        self.data = {
            "token_id": tokenId,
            "model_id": modelId,
            "user_id": userId,
            "client_id": clientId,
            "created_datetime": datetime.today(),
            "timestamp": time(),
            "answers": {},
            "is_alertable": False,
            "alert_status": self.ALERT_STATUS_PENDING,
            }
        return self.data

    def addAnswer(self, questionId, value, timeSpent):
        self.data['answers'][questionId] = {"value": value, "time_spent": timeSpent}

