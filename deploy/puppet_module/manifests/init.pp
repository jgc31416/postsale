class myc {
  
  #Needed packages
  $compilepkgs = [ "nginx-full", "python-pip", "python-support", "python-virtualenv", "python-gevent", "python-pastedeploy", "python-setproctitle",
    "mongodb-server",  "build-essential", "python-dev", "python-numpy", "wget", "mysql-server-5.5", "mysql-client-5.5","libmysqlclient-dev", "git",
    "gunicorn", "python-nose"
   ]
  
  package { $compilepkgs: ensure => "installed" }
  
  file {"/root/install_environment.sh":
          owner => "root",
          group => "root",
          mode => 0755,
          source => "puppet:///modules/myc/install_environment.sh",
  }
  
  # Start Gunicorn install
  exec{"install_environment":
      command => "/root/install_environment.sh",
      require => File["/root/install_environment.sh"],
  }
  
  # Copy modified GUnicorn init script
  file {"/usr/share/gunicorn/initscript-helper.py":
          owner => "root",
          group => "root",
          mode => 0755,
          source => "puppet:///modules/myc/initscript-helper.py",
  }

  # Copy GUnicorn config file
  file {"/etc/gunicorn.d/meetyourcustomers.py":
          owner => "root",
          group => "root",
          mode => 0755,
          source => "puppet:///modules/myc/meetyourcustomers.py",
  }

  # Copy deployment keys
  file {"/home/jesus/.ssh/codepush_rsa":
          owner => "jesus",
          group => "jesus",
          mode => 0600,
          source => "puppet:///modules/myc/codepush_rsa",
  }
  file {"/home/jesus/.ssh/codepush_rsa.pub":
          owner => "jesus",
          group => "jesus",
          mode => 0644,
          source => "puppet:///modules/myc/codepush_rsa.pub",
  }
  file {"/home/jesus/.ssh/config":
          owner => "jesus",
          group => "jesus",
          mode => 0640,
          source => "puppet:///modules/myc/ssh_config",
  }

  # Copy deployment keys
  file {"/root/.ssh/codepush_rsa":
          owner => "root",
          group => "root",
          mode => 0600,
          source => "puppet:///modules/myc/codepush_rsa",
  }
  file {"/root/.ssh/codepush_rsa.pub":
          owner => "root",
          group => "root",
          mode => 0644,
          source => "puppet:///modules/myc/codepush_rsa.pub",
  }
  file {"/root/.ssh/config":
          owner => "root",
          group => "root",
          mode => 0640,
          source => "puppet:///modules/myc/ssh_config",
  }

  # Copy code clone
  file {"/root/code_clone.sh":
          owner => "root",
          group => "root",
          mode => 0755,
          source => "puppet:///modules/myc/code_clone.sh",
  }

  # Code checkout
  exec{"git_clone":
    command => "/root/code_clone.sh",
    require => [File["/root/code_clone.sh"], Package["git"]]
  }

  # Configure nginx
  file {"/root/install_nginx.sh":
          owner => "root",
          group => "root",
          mode => 0755,
          source => "puppet:///modules/myc/install_nginx.sh",
  }

  # Configure nginx
  file {"/root/nginx.tgz":
          mode => 0755,
          source => "puppet:///modules/myc/nginx.tgz",
  }

  # Start ngingx install
  exec{"install_nginx":
      command => "/root/install_nginx.sh",
      require => [File["/root/install_nginx.sh"],File["/root/nginx.tgz"]],
  }

  # Ensure service is up
    service { nginx:
      enable => true,
      ensure => true,
      require => Exec["install_environment"]
    }
  
  # Ensure service is up
    service { gunicorn:
      enable => true,
      ensure => true,
      require => Exec["install_environment"]
    }
  
  # Prepare database
  service { mysql:
      enable => true,
      ensure => true,
      require => Package["mysql-server-5.5"]
  }
  file {"/root/db_creation.sh":
          mode => 0755,
          source => "puppet:///modules/myc/db_creation.sh",
          require => File["/root/db_creation.sql"],
  }
  file {"/root/db_creation.sql":
          mode => 0755,
          source => "puppet:///modules/myc/db_creation.sql",
          require => Package["mysql-server-5.5"],
  }
  exec{"install_db":
      command => "/root/db_creation.sh",
      require => [File["/root/db_creation.sql"], Service['mysql']],
  }


  # Configure mongo

  # Ensure service is up
    service { mongodb:
      enable => true,
      ensure => true,
      require => Exec["install_environment"]
    }
  file {"/etc/mongodb.conf":
          mode => 0744,
          source => "puppet:///modules/myc/mongodb.conf",
          require => Package["mongodb-server"],
  }


  # Install cron jobs

  cron { FillActivity:
    command => "/usr/bin/python /var/www/myc/scripts/QuestionaireFiller.py",
    user    => root,
    hour    => 2,
    minute  => 0,
    require => Service['mongodb'],
  }

  cron { MapReduceJobs1:
    command => "/usr/bin/python /var/www/myc/lib/stats/cron/mrcron.py countResults",
    user    => root,
    hour    => 2,
    minute  => 10,
    require => Service['mongodb'],
  }

  cron { MapReduceJobs2:
    command => "/usr/bin/python /var/www/myc/lib/stats/cron/mrcron.py averageResults",
    user    => root,
    hour    => 2,
    minute  => 20,
    require => Service['mongodb'],
  }

  cron { BatchJobs:
    command => "/usr/bin/python /var/www/myc/lib/stats/cron/batchcron.py",
    user    => root,
    hour    => 2,
    minute  => 30,
    require => Service['mongodb'],
  }

  exec{"prepare_cron":
      command => "/root/prepare_cron.sh",
      require => [File['/root/prepare_cron.sh']],
  }

  file {"/root/prepare_cron.sh":
          mode => 0755,
          source => "puppet:///modules/myc/prepare_cron.sh",
          require => [Service['nginx'], Service['mongodb']],
  }

}  