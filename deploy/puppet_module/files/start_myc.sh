#!/bin/bash
set -e
LOGFILE=/var/log/gunicorn/myc.log
LOGDIR=$(dirname $LOGFILE)
NUM_WORKERS=3
# user/group to run as
USER=jesus
GROUP=jesus
cd /var/www/myc/webapp/
test -d $LOGDIR || mkdir -p $LOGDIR
exec /usr/local/bin/gunicorn_django -w $NUM_WORKERS  --daemon \
    --user=$USER --group=$GROUP --log-level=debug \
    --log-file=$LOGFILE 2>>$LOGFILE 
