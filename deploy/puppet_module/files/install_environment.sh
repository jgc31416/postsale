#!/bin/sh
if [ ! -d /var/www/myc  ]
  then 
	mkdir /var/www/
	mkdir /var/www/myc
	chown jesus:jesus /var/www/ -R
	cd /var/www
	pip install distribute --upgrade
	pip install MySQL-python 
	pip install gunicorn django pymongo django-debug-toolbar
	pip install django-account django_forms_bootstrap django-urlauth django-user-accounts
	pip install pandas
fi

exit 0