#!/bin/sh
if [ ! -e /etc/nginx/sites-available/meetyourcustomers ]
  then
  cd /root/
  tar xfz nginx.tgz
  cp -R etc/nginx/* /etc/nginx/
  rm -fr etc/
  /etc/init.d/nginx restart
fi
exit 0