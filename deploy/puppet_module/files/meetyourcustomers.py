CONFIG = {
    'mode': 'django',
    'environment': {
        'PYTHONPATH': '/var/www/myc/webapp/:/var/www/myc/',
    },
    'working_dir': '/var/www/myc/webapp/',
    'user': 'jesus',
    'group': 'jesus',
    'args': (
        '--bind=127.0.0.1:8000',
        '--workers=4',
        '--timeout=30',
	'--log-level=debug',
	'--log-file=/var/log/gunicorn/myc.log'
    ),
}
