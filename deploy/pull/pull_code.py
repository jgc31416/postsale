'''
Created on Apr 19, 2013

@author: jesus
'''

import subprocess
from os import chdir


def sendMail(msgString, isException = True):
    print msgString

def execute(command, resultString):
    try:
        resultString += "\nExecuting %s" % command
        returnString = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
        resultString += returnString
    except subprocess.CalledProcessError as exc:
        sendMail("Command %s failed \n %s" % (command, exc.output))
        raise
    
if __name__ == '__main__':
    
    repo = "git@bitbucket.org:jgc31416/"
    projectName = "postsale.git"
    tmpDir = "/tmp/" + projectName
    destDir = "/var/www/myc/"
    
    resultString = ""
    
    try:
        #Get the code into temp directory get a shallow copy
        #Clear out directory first
        command = 'rm -fr %s' % (tmpDir)
        execute(command, resultString)
        command = 'git clone --depth 1 %s %s' % (repo+projectName,tmpDir)
        execute(command, resultString)
        
        #Do the unit testing
        chdir(tmpDir) 
        command = 'nosetests lib/tests/'
        execute(command, resultString)
            
        #Do the integration testing (Django+integration)
        command = 'python webapp/manage.py test stats survey api'
        execute(command, resultString)

        #Sync the database 
        command = 'python webapp/manage.py syncdb'
        execute(command, resultString)

        #Copy over the code
        command = 'cp -R %s/* %s' % (tmpDir, destDir)
        execute(command, resultString)
        
        #Restart gunicorn
        command = "service gunicorn restart"
        execute(command, resultString)
        
        sendMail(resultString, isException = False)
    except:
        sendMail("Something has gone wrong")
        exit(-1)
